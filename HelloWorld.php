<?php

class HelloWorld
{
    /**
     * @var PDO
     */

    public function __construct()
    {
    }

    public function hello($what = 'World')
    {
        return "Hello $what!";
    }

}
