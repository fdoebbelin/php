Beispiel PHP-Projekt
===================

Der Zweck dieses Repositorys ist es, zu zeigen, wie man GitLab verwendet, um
Kontinuierliche Integration mit einem PHP-Projekt. Es dient als Begleitprojekt für das
<https://docs.gitlab.com/ce/ci/examples/php.html>.

Um dieses Projekt laufen zu lassen, muss es nur auf GitLab.com geteilt werden.
Jeder Push löst dann einen neuen Build auf GitLab aus.

Quelle
------
Dieses Projekt stammt von: https://github.com/travis-ci-examples/php
